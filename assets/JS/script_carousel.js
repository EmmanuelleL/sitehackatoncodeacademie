var $origin = $("#carouselPlus .carousel-inner").prop("outerHTML");
function multiCarousel(){
	if ( $( "#lg" ).is( ":visible" ) ) {
		do {
			$( "#carouselPlus .carousel-inner" ).children( ".carousel-grid:lt(2)" ).wrapAll( "<div class=\"carousel-item\"><div class=\"row\"></div></div>" );
			$( "#carouselPlus .carousel-inner .carousel-item:first" ).addClass("active");
		} while ( $( "#carouselPlus .carousel-inner" ).children( ".carousel-grid" ).length );

		$('#goldDiv').css('height','100%');
		$('#silverDiv').css('height','100%');
		if ($('#goldDiv').css('height') < $('#silverDiv').css('height'))
			{$('#goldDiv').css('height',$('#silverDiv').css('height'))}
		else {$('#silverDiv').css('height',$('#goldDiv').css('height'))}

	} else if ( $( "#md" ).is( ":visible" ) ) {
		do {
			$( "#carouselPlus .carousel-inner" ).children( ".carousel-grid:lt(2)" ).wrapAll( "<div class=\"carousel-item\"><div class=\"row\"></div></div>" );
			$( "#carouselPlus .carousel-inner .carousel-item:first" ).addClass("active");
		} while ( $( "#carouselPlus .carousel-inner" ).children( ".carousel-grid" ).length );

		$('#goldDiv').css('height','100%');
		$('#silverDiv').css('height','100%');
		if ($('#goldDiv').css('height') < $('#silverDiv').css('height'))
			{$('#goldDiv').css('height',$('#silverDiv').css('height'))}
		else {$('#silverDiv').css('height',$('#goldDiv').css('height'))}

	} else {
		do {
			$( "#carouselPlus .carousel-inner" ).children( ".carousel-grid:lt(1)" ).wrapAll( "<div class=\"carousel-item\"><div class=\"row\"></div></div>" );
			$( "#carouselPlus .carousel-inner .carousel-item:first" ).addClass("active");
		} while ( $( "#carouselPlus .carousel-inner" ).children( ".carousel-grid" ).length);

		$('#goldDiv').css('height','100%');
		$('#silverDiv').css('height','100%');
	}
}
$(window).on( "load resize", function() {
	$.when(
		$( "#carouselPlus .carousel-inner" ).replaceWith( $origin ),
		multiCarousel()
	).done(function() {
		$( ".multi-carousel" ).animate({opacity: "1"}, 1000);
	});
});



