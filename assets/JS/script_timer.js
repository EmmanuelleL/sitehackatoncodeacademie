let secLeft;
let minLeft;
let hoursLeft;
let daysLeft;

var dec = 31;
var jan = 31;
var feb = 28;
var mar = 27

/* Function used to calculate the time left and show it */
function changeTime () {
	var d = new Date();
	var year = d.getFullYear();
	var month = d.getMonth();
	var day = d.getDate();
	var hour = d.getHours();
	var min = d.getMinutes();
	var sec = d.getSeconds();

	if (year > 2019 || (year == 2019 && month > 2) || (year == 2019 && month == 2 && day >= 28)) {
		timesup(year == 2019 && month == 2 && day <= 30)
	}
	else {
		if (year < 2019) {daysLeft = (dec - day) + jan + feb + mar}
		else {
			switch (month) {
				case 0:
					daysLeft = jan - day + feb + mar;
					break;
				case 1:
					daysLeft = feb - day + mar;
					break;
				case 2:
					daysLeft = mar - day;
					break;
			}
		}
		hoursLeft = 23 - hour;
		minLeft = 59 - min;
		secLeft = 59 - sec;

		$('#jours').html(daysLeft);
		if (hoursLeft < 10) {$('#heures').html('0' + hoursLeft);}
		else {$('#heures').html(hoursLeft);}
		if (minLeft < 10) {$('#min').html('0' + minLeft);}
		else {$('#min').html(minLeft);}
		if (secLeft < 10) {$('#sec').html('0' + secLeft);}
		else {$('#sec').html(secLeft);}
	}

}

changeTime();
var timerInterval = setInterval(changeTime, 1000);

/* TODO: function to display a message when the time is up */
function timesup (itsHackathonTime) {
	// Stop the timer
	clearInterval(timerInterval);

	//Write a message, depending on whether it's still the hackathon or not
	if (itsHackathonTime){
		$('#timer').html("C'est l'heure du code !");
	}
	else {
		$('#timer').html("C'est fini !");
	}

	$('#btnInscr').html('Réalisations');
	$('#btnInscr').attr('href', '#realisations0');
	
}