function dropdownMenu() {
  var a = document.getElementById('mobile-menu');
  if (a.classList.contains('menuIcon')) {
    a.classList.remove('menuIcon');
    a.classList.add('menuIcon2');
  }
  else {
    a.classList.remove('menuIcon2');
    a.classList.add('menuIcon');
  }
  document.getElementById("menu").classList.toggle("show");
}

window.onclick = function(event) {
  var a = document.getElementById('mobile-menu');
  if (!event.target.matches('.dropbtn') && !event.target.matches('.fa-bars')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
        a.classList.add('menuIcon');
        a.classList.remove('menuIcon2');
      }
    }
  }

}